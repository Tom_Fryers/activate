"""
Functions for serialising and deserialising objects to JSON.

The JSON can optionally be gzipped.
"""

import gzip
import uuid
from datetime import datetime, timedelta
from pathlib import Path

import numpy as np
import orjson


def default(obj):
    """
    Turn datetimes and timedeltas into JSON.

    >>> default(datetime(2000, 1, 2, 12, 30, 42))
    {'__DATETIME': '2000-01-02T12:30:42'}
    >>> default(timedelta(minutes=1, seconds=40))
    {'__TIMEDELTA': 100.0}
    """
    if isinstance(obj, timedelta):
        return obj.total_seconds()
    if isinstance(obj, Path):
        return str(obj)
    msg = f"Cannot serialise {obj.__class__.__qualname__}"
    raise TypeError(msg)


DECODE = {
    datetime: datetime.fromisoformat,
    timedelta: lambda v: timedelta(seconds=v),
    uuid.UUID: uuid.UUID,
    Path: Path,
    np.array: np.array,
}


def dumps(obj, readable=False):
    """Convert an object to a JSON string."""
    return orjson.dumps(
        obj,
        default=default,
        option=orjson.OPT_SERIALIZE_NUMPY | (orjson.OPT_INDENT_2 if readable else 0),
    )


def dump_bytes(obj, gz=False, readable=False):
    """Convert an object to data."""
    data = dumps(obj, readable=readable)
    return gzip.compress(data) if gz else data


def loads(data, gz=False):
    """Load an object from a string or bytes."""
    data = gzip.decompress(data) if gz else data
    return orjson.loads(data)


def dump(obj, filename, *args, **kwargs):
    """
    Save obj as a JSON file. Can store datetimes and timedeltas.

    Can be gzipped if gz is True.
    """
    with open(filename, "wb") as f:
        f.write(dump_bytes(obj, *args, **kwargs))


def load(filename: Path, gz="auto"):
    """Load a JSON file. Can retrieve datetimes and timedeltas."""
    if gz == "auto":
        gz = filename.suffix.casefold() == ".gz"
    with (gzip.open if gz else open)(filename, "rb") as f:
        return orjson.loads(f.read().replace(b"NaN", b"null"))
