import csv
import string
import unicodedata
from collections import namedtuple
from collections.abc import Iterable
from datetime import datetime, timedelta
from functools import wraps
from pathlib import Path
from zipfile import ZipFile

import activate
from activate import files, load_activity
from activate.activity import Activity
from activate.app import paths


def empty_none(function):
    @wraps(function)
    def inner(arg):
        if arg == "":
            return None
        return function(arg)

    return inner


MONTHS = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12,
}


def parse_strava_datetime(strava_datetime: str) -> datetime:
    date, time = strava_datetime.split(", ")
    day, month, year = date.split(" ")
    hour, minute, second = time.split(":")
    return datetime(
        year=int(year),
        month=MONTHS[month],
        day=int(day),
        hour=int(hour),
        minute=int(minute),
        second=int(second),
    )


def make_identifiers(identifiers: Iterable[str]) -> list[str]:
    new_identifiers = []
    for identifier in identifiers:
        new = "".join(
            c if c in string.ascii_lowercase or c in string.digits and i else "_"
            for i, c in enumerate(unicodedata.normalize("NFKD", identifier.casefold()))
        ).lstrip("_" + string.digits)
        while new in new_identifiers:
            new += "_again"
        new_identifiers.append(new)
    return new_identifiers


def get_track(filename: str, data: bytes):
    path = Path(filename)
    save_path = files.encode_name(path.name, paths.TRACKS)
    save_path.write_bytes(data)
    return load_activity.load(save_path)["track"], save_path


def parse_csv_float(value: str) -> float | None:
    if value == "":
        return None
    return float(value.replace(",", ""))


# TODO SyncState
def import_all_from_export(path):
    """
    Import all activities from a Strava export.

    Returns and iterator. The first item gives the number of activities,
    and the subsequent items are Activities.
    """
    with ZipFile(path) as export_zip:
        lines = export_zip.read("activities.csv").decode("utf-8").splitlines()
        yield len(lines) - 1
        activities = csv.reader(lines)
        activity_row = namedtuple("activity_row", make_identifiers(next(activities)))
        for activity in (activity_row(*a) for a in activities):
            start_time = parse_strava_datetime(activity.activity_date)
            distance = parse_csv_float(
                activity.distance_again
                if activity.distance_again != ""
                else activity.distance
            )
            if activity.filename == "":
                track = activate.track.ManualTrack(
                    start_time=start_time,
                    length=distance,
                    ascent=parse_csv_float(activity.elevation_gain),
                    elapsed_time=empty_none(lambda t: timedelta(seconds=int(t)))(
                        activity.elapsed_time
                    ),
                )
                original_name = None
            else:
                try:
                    track, original_name = get_track(
                        activity.filename, export_zip.read(activity.filename)
                    )
                except Exception as e:
                    # TODO Handle
                    print(activity.filename)
                    print(e)
                    continue
            try:
                photos = activity.media.split("|")
                new_photos = []
                for photo_path in photos:
                    new_photo_path = files.encode_name(Path(photo_path).name, paths.PHOTOS)
                    new_photo_path.write_bytes(export_zip.read(photo_path))
                    new_photos.append(new_photo_path)
                activity = Activity(
                    name=activity.activity_name,
                    sport=load_activity.convert_activity_type(
                        activity.activity_type, activity.activity_name
                    ),
                    track=track,
                    original_name=original_name,
                    flags={"Commute": activity.commute == "true"}
                    if activity.commute
                    else {},
                    effort_level=empty_none(lambda ef: int(float(ef)))(
                        activity.perceived_exertion
                    ),
                    start_time=start_time,
                    distance=distance,
                    description=activity.activity_description,
                    photos=new_photos
                )
            except Exception as e:
                print(activity.filename)
                print(e)
                continue
            yield activity
