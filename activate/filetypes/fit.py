"""Load and parse FIT files."""

import gzip

from fitparse import FitFile

CONVERSION_FACTOR = 180 / 2**31
UNIVERSAL_FIELDS = {"time": lambda p: p["timestamp"], "dist": lambda p: p["distance"]}
NORMAL_FIELDS = {
    **UNIVERSAL_FIELDS,
    "lat": lambda p: p["position_lat"] * CONVERSION_FACTOR,
    "lon": lambda p: p["position_long"] * CONVERSION_FACTOR,
    "ele": lambda p: p["altitude"] if "altitude" in p else p["enhanced_altitude"],
    "cadence": lambda p: p["cadence"] / 60,
    "heartrate": lambda p: p["heart_rate"] / 60,
    "speed": lambda p: p["speed"] if "speed" in p else p["enhanced_speed"],
    "power": lambda p: p["power"],
}

LENGTH_SWIM_FIELDS = {
    **UNIVERSAL_FIELDS,
    "speed": lambda p: p["speed"],
    "stroke": lambda p: p["swim_stroke"],
}


def load(filename):
    """Load and parse a FIT file."""
    if filename.suffix == ".gz":
        with gzip.open(filename) as f:
            fit = FitFile(f).messages
    else:
        fit = FitFile(str(filename)).messages
    fit = list(fit)
    for message in fit:
        point = message.get_values()
        if point.items() & {"sub_sport": "lap_swimming", "event": "length"}.items():
            return parse_fit(filename, fit, LENGTH_SWIM_FIELDS)
    return parse_fit(filename, fit, NORMAL_FIELDS)


def parse_fit(filename, fit, available_fields):
    """Extract the useful fields from a FIT file."""
    sport = "unknown"
    fields = {field: [] for field in available_fields}
    for message in fit:
        point = message.get_values()
        if message.mesg_type is None:
            continue
        if message.mesg_type.name == "session":
            sport = point["sport"]

        elif message.mesg_type.name == "record":
            for field, function in available_fields.items():
                try:
                    value = function(point)
                except Exception:
                    value = None
                fields[field].append(value)
    fields = {
        field: values
        for field, values in fields.items()
        if any(v is not None for v in values)
    }
    return None, sport, fields
