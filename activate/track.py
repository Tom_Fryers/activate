"""Contains the Track class and functions for handling tracks."""

from __future__ import annotations

import bisect
from collections import defaultdict
from dataclasses import asdict, dataclass
from datetime import datetime, timedelta
from functools import cached_property
from itertools import tee
from math import dist

import numpy as np
from dtw import dtw

from activate import geometry, serialise, times
from activate.units import DimensionValue

SPEED_RANGE = 1

FIELD_DIMENSIONS = {
    "lat": "latlon",
    "lon": "latlon",
    "ele": "altitude",
    "height_change": "altitude",
    "vertical_speed": "vertical_speed",
    "climb": "altitude",
    "desc": "altitude",
    "gradient": None,
    "angle": "angle",
    "time": "time",
    "speed": "speed",
    "dist": "distance",
    "dist_to_last": "distance",
    "cadence": "cadence",
    "heartrate": "heartrate",
    "power": "power",
}

DTYPES: defaultdict[str, type] = defaultdict(
    lambda: np.float64, {"time": np.datetime64}
)
SEC = np.timedelta64(1, "s")


class InvalidTrackError(Exception):
    pass


def timestamp(time: np.datetime64):
    return time.astype("datetime64[s]").astype(int)


def lerp(value1, value2, ratio):
    """
    Interpolate between value1 and value2.

    lerp(x, y, 0) = x
    lerp(x, y, 0.5) = (x + y) / 2
    lerp(x, y, 1) = y
    """
    return value1 + ratio * (value2 - value1)


try:
    from itertools import pairwise
except ImportError:

    def pairwise(iterator):
        """Return pairs of adjacent items."""
        firsts, seconds = tee(iterator, 2)
        next(seconds, None)
        return zip(firsts, seconds)


def infer_nones(data):
    """Infer None values by linear interpolation."""
    nans = np.isnan(data)
    data[nans] = np.interp(nans.nonzero()[0], (~nans).nonzero()[0], data[~nans])


def get_nearby_indices(length, position, number=1) -> range:
    """
    Return numbers around position, with at most number either side.

    If position is too close to 0 or length, the excess points are
    removed.
    """
    return range(max(position - number, 0), min(position + number + 1, length))


def enumerate_from(list_, point):
    return enumerate(list_[point:], point)


@dataclass
class ManualTrack:
    """A manual track with a few basic values."""

    start_time: datetime
    length: float
    ascent: float
    elapsed_time: timedelta

    has_altitude_data = False
    has_position_data = False
    manual = True

    def average(self, field):
        if field == "speed":
            try:
                return self.length / self.elapsed_time.total_seconds()
            except ZeroDivisionError:
                return None
        msg = f"{self.__class__.__qualname__} has no average {field}"
        raise AttributeError(msg)

    @property
    def save_data(self):
        result = asdict(self)
        result["manual"] = True
        return result

    @classmethod
    def from_serial(cls, serial):
        del serial["manual"]
        serial["start_time"] = serialise.DECODE[datetime](serial["start_time"])
        serial["elapsed_time"] = serialise.DECODE[timedelta](serial["elapsed_time"])
        return cls(**serial)

    @property
    def end_time(self):
        return self.start_time + self.elapsed_time

    @property
    def distance_in_days(self):
        """Estimate distance each day, assuming constant speed."""
        if self.start_time.date() == self.end_time.date():
            return {self.start_time.date(): self.length}
        totals = {self.start_time.date(): 0}
        speed = self.average("speed")
        totals[self.start_time.date()] += (
            speed * (times.end_of(self.start_time, "day") - self.start_time) / SEC
        )
        for days in range(1, (self.end_time.date() - self.start_time.date()).days):
            day = self.start_time.date() + timedelta(days)
            totals[day] = speed * timedelta(days=1)
        totals[self.end_time.date()] = (
            speed * (self.end_time - times.start_of(self.end_time, "day")) / SEC
        )
        return totals

    def get_curve(self, table_distances):
        """Get the curve and curve table for an activity."""
        table_distances = [x for x in table_distances if x <= self.length]
        time = self.elapsed_time.total_seconds()
        speeds = [distance / time if time else 0 for distance in table_distances]
        bests_table = [
            (
                DimensionValue(distance, "distance"),
                DimensionValue(self.elapsed_time, "time"),
                DimensionValue(speed, "speed"),
            )
            for distance, speed in zip(table_distances, speeds)
        ]
        return (bests_table, ((table_distances, "distance"), (speeds, "speed")), None)

    def __contains__(self, _):
        return False


class Track:
    """
    A series of GPS points at given times.

    A track is considered to be purely a sequence of GPS points, with
    extra data for each point. For more metadata such as a name or
    description, the Track should be wrapped in an Activity.

    Some tracks (those representing pool swims) have no position data.
    """

    manual = False

    def __init__(self, fields):
        self.base_fields = tuple(fields)
        self.fields = {
            name: np.array(data, DTYPES[name], copy=False)
            for name, data in fields.items()
        }
        for essential in ("lat", "lon"):
            if essential in self.fields:
                infer_nones(self[essential])

        if "time" in self.fields:
            as_float = self["time"].astype("float")
            as_float[np.isnat(self["time"])] = np.nan
            infer_nones(as_float)
            self["time"] = as_float.astype(self["time"].dtype)

        if self.has_altitude_data:
            infer_nones(self["ele"])
        else:
            self["ele"] = np.zeros(len(self))

    def __getitem__(self, field):
        try:
            return self.fields[field]
        except KeyError:
            if field == "dist_to_last":
                self.calculate_dist_to_last()
            elif field == "time_to_last":
                self.calculate_time_to_last()
            elif field == "dist":
                self.calculate_dist()
            elif field == "speed":
                self.calculate_speed()
            elif field in {"climb", "desc"}:
                self.calculate_climb_desc()
            elif field == "height_change":
                self.calculate_height_change()
            elif field == "vertical_speed":
                self.calculate_vertical_speed()
            elif field == "gradient":
                self.calculate_gradient()
            elif field == "angle":
                self.calculate_angle()
        return self.fields[field]

    def __setitem__(self, field, value):
        self.fields[field] = value

    def __contains__(self, field):
        if field in {"dist_to_last", "dist"}:
            return "dist" in self or "dist_to_last" in self or self.has_position_data
        if field in {
            "climb",
            "desc",
            "height_change",
            "vertical_speed",
            "gradient",
            "angle",
        }:
            return self.has_altitude_data
        return field in self.fields

    @cached_property
    def temporal_resolution(self):
        return min(self["time_to_last"])

    @cached_property
    def xyz(self):
        return np.array(
            [
                geometry.to_cartesian(*point)
                for point in zip(self["lat"], self["lon"], self["ele"])
            ]
        )

    def calculate_dist_to_last(self):
        """Calculate distances between adjacent points."""
        if "dist" in self.fields:
            self.fields["dist_to_last"] = np.diff(self.fields["dist"])
        elif self.has_position_data:
            self.fields["dist_to_last"] = np.fromiter(
                (dist(*x) for x in pairwise(self.xyz)), float, count=len(self) - 1
            )
        else:
            msg = "Activity has no distance information"
            raise InvalidTrackError(msg)

    def calculate_time_to_last(self):
        """Calculate durations between adjacent points."""
        self.fields["time_to_last"] = np.diff(self["time"]) / SEC

    def calculate_climb_desc(self):
        self.fields["climb"] = np.maximum(self["height_change"], 0)
        self.fields["desc"] = -np.minimum(self["height_change"], 0)

    def calculate_dist(self):
        """Calculate cumulative distances."""
        self.fields["dist"] = np.insert(np.cumsum(self["dist_to_last"]), 0, 0)

    def calculate_speed(self):
        """Calculate speeds at each point."""
        self["speed"] = self["dist_to_last"] / self["time_to_last"]

    def calculate_height_change(self):
        """Calculate differences in elevation between adjacent points."""
        self.fields["height_change"] = []
        last_ele = self["ele"][0]
        last_index = 0
        index = 1
        while True:
            while index < len(self) - 1 and self["ele"][index] == last_ele:
                index += 1
            ele = self["ele"][index]
            self.fields["height_change"] += [
                (ele - last_ele) / (index - last_index)
            ] * (index - last_index)
            last_index = index
            last_ele = ele
            index += 1
            if index >= len(self):
                self.fields["height_change"] = np.array(
                    self.fields["height_change"], dtype=np.float64
                )
                return

    def calculate_vertical_speed(self):
        """Calculate vertical speed at each point."""
        self.fields["vertical_speed"] = (
            self.fields["height_change"] / self.fields["time_to_last"]
        )

    def calculate_gradient(self):
        """Calculate the gradient at each point."""
        self.fields["gradient"] = self["height_change"] / self["dist_to_last"]

    def calculate_angle(self):
        """Calculate the angle of inclination at each point."""
        self.fields["angle"] = np.arctan(self["gradient"])

    def __len__(self):
        return len(next(iter(self.fields.values())))

    def without_nones(self, field):
        return self[field][~np.isnan(self[field])]

    def average(self, field):
        """Get the mean value of a field, ignoring missing values."""
        values = self[field]
        if len(values) == len(self["time_to_last"]):
            return np.nansum(self[field] * self["time_to_last"]) / np.sum(
                self["time_to_last"][~np.isnan(values)]
            )

        return (
            np.nansum(values[:-1] * self["time_to_last"])
            + np.nansum(values[1:] * self["time_to_last"])
        ) / np.sum(
            self["time_to_last"]
            * (
                2
                - (np.isnan(values[:-1]).astype(int) + np.isnan(values[1:]).astype(int))
            )
        )

    def maximum(self, field):
        """Get the maximum value of a field, ignoring missing values."""
        return np.nanmax(self[field])

    # Caching necessary to avoid fake elevation data
    @cached_property
    def has_altitude_data(self):
        return "ele" in self.fields

    @cached_property
    def has_position_data(self):
        return "lat" in self.fields and "lon" in self.fields

    @cached_property
    def lat_lon_list(self):
        return [[x, y] for x, y in zip(self["lat"], self["lon"])]

    def part_lat_lon_list(self, min_dist, max_dist):
        min_i = 0
        for i, dis in enumerate(self["dist"]):
            if dis is None:
                continue
            if dis < min_dist:
                min_i = i
            if dis >= max_dist:
                max_i = i
                break
        return self.lat_lon_list[min_i + 1 : max_i + 1]

    @cached_property
    def ascent(self):
        if self.has_altitude_data:
            return np.nansum(self["climb"])
        return None

    @cached_property
    def descent(self):
        if self.has_altitude_data:
            return np.nansum(self["desc"])
        return None

    @property
    def start_time(self):
        return self["time"][0].item()

    @property
    def end_time(self):
        return self["time"][-1].item()

    @cached_property
    def elapsed_time(self) -> timedelta:
        return self.end_time - self.start_time

    @cached_property
    def moving_time(self) -> timedelta:
        total_time = 0
        time = 0
        distance = 0
        for distance_difference, time_difference in zip(
            self["dist_to_last"], self["time_to_last"]
        ):
            distance_difference = np.nan_to_num(distance_difference)
            time += time_difference
            distance += distance_difference
            if not time_difference or distance < 1:
                continue
            if distance_difference / time_difference > 0.2:
                total_time += time
            time = 0
            distance = 0
        return timedelta(seconds=total_time)

    @cached_property
    def average_speed_moving(self):
        return self.length / self.moving_time.total_seconds()

    @cached_property
    def distance_in_days(self) -> dict:
        if self.start_time.date() == self.end_time.date():
            return {self.start_time.date(): self.length}
        last_time = self.start_time
        last_date = last_time.date()
        totals = {last_date: 0}

        for dist_to_last, time in zip(
            self["dist_to_last"][1:], (t for t in self["time"][1:])
        ):
            if dist_to_last is None:
                continue
            date = time.item().date()
            if date == last_date:
                totals[last_date] += dist_to_last
            else:
                speed = dist_to_last / ((time - last_time) / SEC)
                totals[last_date] += (
                    speed
                    * (
                        times.end_of(last_time.item(), "day") - last_time.item()
                    ).total_seconds()
                )
                for days in range(1, (date - last_date).days):
                    day = last_date + timedelta(days)
                    totals[day] = speed * timedelta(days=1)
                totals[date] = (
                    speed
                    * (time.item() - times.start_of(time.item(), "day")).total_seconds()
                )
                last_date = date

            last_time = time
        return totals

    def flat_equivalent_distance(self, model):
        # https://journals.physiology.org/doi/full/10.1152/japplphysiol.01177.2001
        def energy_cost(g):
            g = np.clip(g, -0.45, 0.45)
            if model == "Run":
                return (
                    155.4 * g**5
                    - 30.4 * g**4
                    - 43.3 * g**3
                    + 46.3 * g**2
                    + 19.5 * g
                    + 3.6
                )
            if model == "Walk":
                return (
                    280.5 * g**5
                    - 58.7 * g**4
                    - 76.8 * g**3
                    + 51.9 * g**2
                    + 19.6 * g
                    + 2.5
                )
            msg = f"invalid model type: {model}"
            raise ValueError(msg)

        total_energy = np.nansum(energy_cost(self["gradient"]) * self["dist_to_last"])
        return total_energy / energy_cost(0)

    def lat_lng_from_distance(self, distance):
        distances = self.without_nones("dist")
        point0 = bisect.bisect(distances, distance)
        try:
            point1 = next(
                i for i, d in enumerate_from(distances, point0) if d > distance
            )
        except StopIteration:
            return None
        point0 -= 1
        dist0 = distances[point0]
        dist1 = distances[point1]
        lat0 = self["lat"][point0]
        lon0 = self["lon"][point0]
        if dist0 == dist1:
            return (lat0, lon0)
        lat1 = self["lat"][point1]
        lon1 = self["lon"][point1]
        ratio = (distance - dist0) / (dist1 - dist0)
        return (lerp(lat0, lat1, ratio), lerp(lon0, lon1, ratio))

    def graph(self, y_data, x_data="dist") -> tuple:
        """Get x and y data as (data, dimension) tuples."""
        return (
            (self[x_data], FIELD_DIMENSIONS[x_data]),
            (self[y_data], FIELD_DIMENSIONS[y_data]),
        )

    @property
    def length(self):
        return next(x for x in reversed(self["dist"]) if not np.isnan(x))

    def splits(self, split_length=1000) -> list[list[DimensionValue]]:
        """
        Split an activity into splits, with per-split data.

        Each split is a list in the format
        [lap, split, speed, net climb, total climb].
        """
        splits: list[list[DimensionValue]] = []
        last_time = None
        last_alt = self["ele"][0]
        total_climb = 0
        for time, distance, alt, climb in zip(
            self["time"], self["dist"], self["ele"], self["climb"]
        ):
            time = time.item()
            if last_time is None:
                last_time = time
                last_alt = alt
            if distance is None:
                continue
            if distance // split_length > len(splits):
                speed = split_length / (time - last_time).total_seconds()
                splits.append(
                    [
                        DimensionValue(time - last_time, "time"),
                        DimensionValue(time - self.start_time, "time"),
                        DimensionValue(speed, "speed"),
                        DimensionValue(alt - last_alt, "altitude"),
                        DimensionValue(total_climb, "altitude"),
                    ]
                )
                total_climb = 0
                last_time = None
            if np.isfinite(climb):
                total_climb += climb

        return splits

    def get_zone_durations(self, zones, field="speed", count_field="time_to_last"):
        """
        Get durations for the zones graph.

        Between all zones values, calculate the total amount of
        count_field at each point where field is within the range given
        by that pair of zones values. The zones must be sorted in
        ascending order.
        """
        indexed_zones = tuple(enumerate(zones))[::-1]
        buckets = [0 for _ in zones]
        zone_indices = [[] for _ in zones]
        for point in range(len(self) - 1):
            duration = self[count_field][point]
            # Add it to the correct bucket
            value = self[field][point]
            if value is None:
                continue
            for i, zone in indexed_zones:
                if value > zone:
                    buckets[i] += duration
                    zone_indices[i].append(point)
                    break

        def to_indexing_array(indices):
            result = [[]]
            for n in indices:
                if n not in result[-1]:
                    result.append(-1)
                    result.append([n])
                result[-1].append(n + 1)
            return [np.array(r) for r in result[1:]]

        return buckets, [to_indexing_array(a) for a in zone_indices]

    def get_curve(self, table_distances):
        """Get the curve and curve table for an activity."""
        table_distances = [x for x in table_distances if x <= self.length][::-1]
        usable = ~np.isnan(self["dist"])
        time_values = timestamp(self["time"][usable])
        dist_values = self["dist"][usable]

        bests = []
        point_indices = []
        for distance in table_distances:
            last_iter = iter(enumerate(dist_values))
            first_iter = iter(enumerate(dist_values))
            last_point = next(i for i, d in last_iter if d >= distance)
            best = time_values[last_point] - time_values[0]
            first_point = 0
            point = (first_point, last_point)
            for last_point, last_dist in last_iter:
                if dist_values[last_point] - dist_values[first_point] < distance:
                    continue
                first_point = (
                    next(p for p, d in first_iter if last_dist - d < distance) - 1
                )
                time_taken = time_values[last_point] - time_values[first_point]
                if time_taken < best:
                    best = time_taken
                    point = (first_point, last_point)
            bests.append(best)
            point_indices.append(point)
            if best == self.temporal_resolution:
                break
        while len(point_indices) < len(table_distances):
            point_indices.append(point_indices[-1])
            bests.append(bests[-1])

        point_indices = point_indices[::-1]
        bests = bests[::-1]
        table_distances = table_distances[::-1]

        speeds = [
            distance / time if time else 0
            for distance, time in zip(table_distances, bests)
        ]
        bests_table = [
            (
                DimensionValue(distance, "distance"),
                DimensionValue(timedelta(seconds=time.item()), "time"),
                DimensionValue(speed, "speed"),
            )
            for distance, time, speed in zip(table_distances, bests, speeds)
        ]
        return (
            bests_table,
            ((table_distances, "distance"), (speeds, "speed")),
            point_indices,
        )

    def match(self, other, tolerance=40):
        return (
            dtw(self.xyz, other.xyz, distance_only=True).normalizedDistance < tolerance
        )

    def max_point(self, stat):
        point = None
        maximum = float("-inf")
        for index, value in enumerate(self[stat]):
            if value is not None and value > maximum:
                maximum = value
                point = index
        return point

    @property
    def save_data(self):
        return {key: self.fields[key] for key in self.base_fields}

    @classmethod
    def from_serial(cls, serial):
        return cls(serial)
